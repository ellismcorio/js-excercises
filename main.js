
// Find the Smallest Number in an Array

// Create a function that takes an array of numbers and returns the smallest number in the set.

// [34, 15, 88, 2] ➞ 2
// [34, -345, -1, 100] ➞ -345
// [-76, 1.345, 1, 0] ➞ -76
// [0.4356, 0.8795, 0.5435, -0.9999] ➞ -0.9999
// [7, 7, 7] ➞ 7

var a = [34, 15, 88, 2];
var b =  [34, -345, -1, 100];
var c =  [-76, 1.345, 1, 0];
var d = [0.4356, 0.8795, 0.5435, -0.9999];
var e =  [7, 7, 7];

console.log("Smallest NUmbers")

a.sort(function(a, b) {
  return a - b;
});
console.log(a[0]);

b.sort(function(a, b) {
  return a - b;
});
console.log(b[0]);

c.sort(function(a, b) {
  return a - b;
});
console.log(c[0]);

d.sort(function(a, b) {
  return a - b;
});
console.log(d[0]);

e.sort(function(a, b) {
  return a - b;
});
console.log(e[0]);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Absolutely Sum
// Take an array of integers (positive or negative or both) and return the sum of the absolute value of each element.

// [2, -1, 4, 8, 10] ➞ 25
// [-3, -4, -10, -2, -3] ➞ 22
// [2, 4, 6, 8, 10] ➞ 30
// [-1] ➞ 1

console.log("Absolutely Sum")

var a = [2, -1, 4, 8, 10];
var b = [-3, -4, -10, -2, -3];
var c = [2, 4, 6, 8, 10];
var d = [-1];


var sumTotala =[];
a.forEach(function(element) {
  sumTotala.push(Math.abs(element));
});
const reducera = (accumulator, currentValue) => accumulator + currentValue;
console.log(sumTotala.reduce(reducera));

var sumTotalb =[];
b.forEach(function(element) {
  sumTotalb.push(Math.abs(element));
});
const reducerb = (accumulator, currentValue) => accumulator + currentValue;
console.log(sumTotalb.reduce(reducerb));

var sumTotalc =[];
c.forEach(function(element) {
  sumTotalc.push(Math.abs(element));
});
const reducerc = (accumulator, currentValue) => accumulator + currentValue;
console.log(sumTotalc.reduce(reducerc));

var sumTotald =[];
d.forEach(function(element) {
  sumTotald.push(Math.abs(element));
});
const reducerd = (accumulator, currentValue) => accumulator + currentValue;
console.log(sumTotald.reduce(reducerd));

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Vending Machine
// Your task is to create a function that simulates a vending machine.

// Given an amount of money  and a productNumber, the vending machine should output the correct product name and give back the correct amount of change.

// The coins used for the change are the following: [500, 200, 100, 50, 20, 10]

// The return value is an object with 2 properties:

// product: the product name that the user selected.
// change: an array of coins (can be empty, must be sorted in descending order).

// vendingMachine(products, 200, 7); ➞ { product: 'Crackers', change: [ 50, 20, 10 ] }
// vendingMachine(products, 500, 0); ➞ 'Enter a valid product number'
// vendingMachine(products, 90, 1); ➞ 'Not enough money for this product'


// Tests
// Test.assertSimilar(vendingMachine(products, 500, 8), { product: 'Potato chips', change: [ 200, 50, 20, 10 ] });
// Test.assertSimilar(vendingMachine(products, 500, 1), { product: 'Orange juice', change: [ 200, 200 ] });
// Test.assertSimilar(vendingMachine(products, 200, 7), { product: 'Crackers', change: [ 50, 20, 10 ] });
// Test.assertSimilar(vendingMachine(products, 100, 9), { product: 'Small snack', change: [ 20 ] });
// Test.assertSimilar(vendingMachine(products, 1000, 6), { product: 'Condoms', change: [ 500 ] });
// Test.assertSimilar(vendingMachine(products, 250, 4), { product: 'Cookies', change: [] });
// Test.assertEquals(vendingMachine(products, 500, 0), 'Enter a valid product number');
// Test.assertEquals(vendingMachine(products, 90, 1), 'Not enough money for this product');
// Test.assertEquals(vendingMachine(products, 0, 0), 'Enter a valid product number');


console.log("Vending Machine");

/////////////////////////////////////////////////////////////////
var product =8; // Please enter a number from the list below.
var cash = 500; // Please enter an amount of money to insert.
/////////////////////////////////////////////////////////////////

const products = [
  { number: 1, price: 100, name: 'Orange juice' },
  { number: 2, price: 200, name: 'Soda' },
  { number: 3, price: 150, name: 'Chocolate snack' },
  { number: 4, price: 250, name: 'Cookies' },
  { number: 5, price: 180, name: 'Gummy bears' },
  { number: 6, price: 500, name: 'Condoms' },
  { number: 7, price: 120, name: 'Crackers' },
  { number: 8, price: 220, name: 'Potato chips' },
  { number: 9, price: 80,  name: 'Small snack' },
];

const coins = [500, 200, 100, 50, 20, 10, 5];

function vendingMachine(product, cash)  {
  if (`${product}` > products.length || `${product}` < 1)
  {
    console.log("Enter a valid product number")
  }
  else if (`${cash}` < products[`${product-1}`].price)
  {
    console.log("Not enough money for this product")
  }
  else if(`${product}` == products[`${product-1}`].number)
  {
    change = [];
    if (`${cash}` > products[`${product-1}`].price )
    {
      var changeTotal = `${cash}` - products[`${product-1}`].price;
      var i;
      while (changeTotal > 0)
      {
        for(i = 0; i < coins.length; i++){
          if(changeTotal >= coins[i])
          { 
            change.push(coins[i]);
            changeTotal -= coins[i];
            break;
          }                
        }
      }
    }
    result = { product: products[`${product-1}`].name, change: change };
    console.log(result);
  }
}

vendingMachine(`${product}`,`${cash}`);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// SimpleCrypt
// You'll need to decrypt some strings found on a website and maybe one of that will help you further to get your ever wanted software.
// Let's do OOP and create an object:

// Create an object named SimpleCrypt with a constructor that will store the original given string.
// SimpleCrypt should have 3 prototypes : encrypt, decrypt, reset.
// Prototype encrypt should take one optional argument (string type) and will encrypt the argument if present or original string if no arguments.
// Prototype decrypt should take one optional argument too (string type) and will decrypt the argument if present or get last string with encrypt.
// Prototype reset should reset all object properties to origin.

// myExemple.encrypt("123456789") ➞ "2468:<>@B"
// myExemple.encrypt("abcdefghij") ➞"bdfhjlnprt"
// myExemple.encrypt("9876543210") ➞ "::::::::::"
// myExemple.encrypt("98754310") ➞":::99988"
// myExemple.encrypt("qwerty") ➞"ryhvyu007f"
// myExemple.encrypt("azerty") ➞"b|hvyu007f"


console.log("SimpleCrypt");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Mike and his friend love playing tic tac toe. In fact, they love it so much they created a multiplayer web version to play remotely against each other. At the last minute Mike realized he forgot to call an animation when a player won the game. Now he needs to create a function that will check it but can't do it himself because he's short on time. Mike needs your help!

// Create a function ticTacToeCheck that takes a two dimensional array representing a finished game of tic tac toe (e.g. [["XO-"], ["XXX"], ["XO-"]]).


// X represents player 1 move(s).
// O represents player 2 move(s).
// Dash (-) represents no move(s).

// 1) Return true if player 1 or player 2 win the game.
var a = [["XO-"], ["XXO"], ["O-X"]]; // ➞ true

// 2) Return false if nobody won the game.
var b = [["XXO"], ["OOX"], ["XOO"]]; // ➞ false

// 3) Return "Nobody moved" if nobody made a move.
var c = [["---"],  ["---"], ["---"]];  // ➞ "Nobody moved

// 4) Return "No/Incomplete game" if no value was received / all received arrays are void / type of any given value is not "array".
var d = [[], [], []]; // ➞ "No/Incomplete game"
var e = [[], []]; // ➞ "No/Incomplete game"
[[]], []; // ➞ "No/Incomplete game" //////////////////////////////////////////////////   this is syntactically incorrect /////////////////////////////////////////////////////////////////////
var f = ["XOX", ["XOX"], ["XOO"]]; // ➞ "No/Incomplete game"
var g = ["XOX", ["XOX"], []]; // ➞ "No/Incomplete game"
var h = undefined; // ➞ "No/Incomplete game"

// 5) Return "Corrupted game" if any values differ from the expected "X", "O", "-".
var i = [["X"], ["XX"], ["X-"]]; // ➞ "Corrupted game"
var j = [["AAA"], ["BBA"], ["ABB"]] //➞ "Corrupted game"
var k = [["XOX"], ["XOO"], [undefined]]; //➞ "Corrupted game"
var l = [["XXX"], ["XOX"], [""]]; // ➞ "Corrupted game"
var m = [["XXX"], ["XOX"], [1,2,3]]; //➞ "Corrupted game"

console.log("tic-tac-toe");

function ticTacToeCheck(game) {
var bIndex;
if (typeof(game) == "undefined")
{
  return "No/Incomplete game";
}
if (game.toString() == "," || game.toString() == ",,") {
  return "No/Incomplete game";
}
if (game.toString() == "---,---,---")
    {
      return "Nobody has Moved";
    }  
    else {
    for(bIndex = 0; bIndex < game.length; bIndex++){
      if (!(typeof(game[bIndex][0] == "object" || "array")))
      {
      return "No/Incomplete game";
      }
      if (typeof(game[bIndex]) == "string")
      {
        return "No/Incomplete game";
      }
      if (game[bIndex][0].length != 3) 
      {
       return "Corrupted game";
      }
      if (!(game[bIndex][0].toString().includes('X','O')))
      {
        return "Corrupted game";
      }
      if (typeof(game[0][0]) == "undefined" || typeof(game[1][0]) == "undefined" || typeof(game[2][0]) == "undefined")
      {
        return "Corrupted game";
      }
    }
  }
  // check left column
  if ( (game[0][0][0] == "X" && game[0][0][1] == "X" && game[0][0][2] == "X") || (game[0][0][0] == "O" && game[0][0][1] == "O" && game[0][0][2] == "O")  )
  {
    if (game[0][0][0] == "X")
    {
      return "X Wins";
    }
    else 
    {
      return "O Wins";
    }
  }
  // check middle column
  if ( (game[1][0][0] == "X" && game[1][0][1] == "X" && game[1][0][2] == "X") || (game[1][0][0] == "O" && game[1][0][1] == "O" && game[1][0][2] == "O")  )
  {
    if (game[1][0][0] == "X")
    {
      return "X Wins";
    }
    else 
    {
      return "O Wins";
    }
  }
  // check right column
  if ( (game[2][0][0] == "X" && game[2][0][1] == "X" && game[2][0][2] == "X") || (game[2][0][0] == "O" && game[2][0][1] == "O" && game[2[0]][2] == "O")  )
  {
    if (game[2][0][0] == "X")
    {
      return "X Wins";
    }
    else 
    {
      return "O Wins";
    }
  }
  // check top row
  if ( (game[0][0][0] == "X" && game[1][0][0] == "X" && game[2][0][0] == "X") || (game[0][0][0] == "O" && game[1][0][0] == "O" && game[2][0][0] == "O")  )
  {
    if (game[0][0][0] == "X")
    {
      return "X Wins";
    }
    else 
    {
      return "O Wins";
    }
  }
   // check middle row
   if ( (game[0][0][1] == "X" && game[1][0][1] == "X" && game[2][0][1] == "X") || (game[0][0][1] == "O" && game[1][0][1] == "O" && game[2][0][1] == "O")  )
   {
     if (game[0][0][1] == "X")
     {
      return "X Wins";
    }
    else 
    {
      return "O Wins";
    }
   }
    // check bottom  row
  if ( (game[0][0][2] == "X" && game[1][0][2] == "X" && game[2][0][2] == "X") || (game[0][0][2] == "O" && game[1][0][2] == "O" && game[2][0][2] == "O")  )
  {
    if (game[0][0][2] == "X")
    {
      return "X Wins";
    }
    else 
    {
      return "O Wins";
    }
  }
   // check diagonal ltr
   if ( (game[0][0][0] == "X" && game[1][0][1] == "X" && game[2][0][2] == "X") || (game[0][0][0] == "O" && game[1][0][1] == "O" && game[2][0][2] == "O")  )
   {
     if (game[0][0][0] == "X")
     {
      return "X Wins";
    }
    else 
    {
      return "O Wins";
    }
   }
    // check diagonal rtl
  if ( (game[2][0][0] == "X" && game[1][0][1] == "X" && game[0][0][2] == "X") || (game[2][0][0] == "O" && game[1][0][1] == "O" && game[0][0][2] == "O")  )
  {
    if (game[2][0][0] == "X")
    {
      return "X Wins";
    }
    else 
    {
      return "O Wins";
    }
  }
  return "No winner";
}

console.log(ticTacToeCheck(a));
console.log(ticTacToeCheck(b));
console.log(ticTacToeCheck(c));
console.log(ticTacToeCheck(d));
console.log(ticTacToeCheck(e));
console.log(ticTacToeCheck(f));
console.log(ticTacToeCheck(g));
console.log(ticTacToeCheck(h));
console.log(ticTacToeCheck(i));
console.log(ticTacToeCheck(j));
console.log(ticTacToeCheck(k));
console.log(ticTacToeCheck(l));
console.log(ticTacToeCheck(m));



